EESchema Schematic File Version 4
LIBS:RobotIM-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Motor:Motor_Servo M?
U 1 1 5EA6E6EF
P 3650 6950
AR Path="/5EA699B1/5EA6E6EF" Ref="M?"  Part="1" 
AR Path="/5EA6A304/5EA6E6EF" Ref="M7"  Part="1" 
F 0 "M7" H 3981 7015 50  0000 L CNN
F 1 "Motor_Servo" H 3981 6924 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B04B-EH-A_1x04_P2.50mm_Vertical" H 3650 6760 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3650 6760 50  0001 C CNN
	1    3650 6950
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_Servo M?
U 1 1 5EA6E6F6
P 3650 5650
AR Path="/5EA699B1/5EA6E6F6" Ref="M?"  Part="1" 
AR Path="/5EA6A304/5EA6E6F6" Ref="M6"  Part="1" 
F 0 "M6" H 3981 5715 50  0000 L CNN
F 1 "Motor_Servo" H 3981 5624 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B04B-EH-A_1x04_P2.50mm_Vertical" H 3650 5460 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3650 5460 50  0001 C CNN
	1    3650 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 5750 3350 5900
Wire Wire Line
	3350 5900 4050 5900
Wire Wire Line
	4050 5900 4050 7200
Wire Wire Line
	3350 7050 3350 7200
Wire Wire Line
	3350 7200 4050 7200
Text Label 1500 2750 0    50   ~ 0
NSS0
Text Label 1500 2850 0    50   ~ 0
SCK
Text Label 1500 2950 0    50   ~ 0
MISO
Text Label 1500 3050 0    50   ~ 0
MOSI
$Comp
L power:GND #PWR0164
U 1 1 5EEED0BB
P 3950 4350
F 0 "#PWR0164" H 3950 4100 50  0001 C CNN
F 1 "GND" H 4000 4150 50  0000 C CNN
F 2 "" H 3950 4350 50  0001 C CNN
F 3 "" H 3950 4350 50  0001 C CNN
	1    3950 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0165
U 1 1 5EEED87B
P 1850 3750
F 0 "#PWR0165" H 1850 3500 50  0001 C CNN
F 1 "GND" H 1900 3550 50  0000 C CNN
F 2 "" H 1850 3750 50  0001 C CNN
F 3 "" H 1850 3750 50  0001 C CNN
	1    1850 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3750 1850 3750
Wire Wire Line
	2150 3650 1850 3650
Wire Wire Line
	1850 3650 1850 3750
Connection ~ 1850 3750
$Comp
L power:GND #PWR0166
U 1 1 5EEEE664
P 4100 3250
F 0 "#PWR0166" H 4100 3000 50  0001 C CNN
F 1 "GND" H 4100 3100 50  0000 C CNN
F 2 "" H 4100 3250 50  0001 C CNN
F 3 "" H 4100 3250 50  0001 C CNN
	1    4100 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0167
U 1 1 5EEEE695
P 4100 3650
F 0 "#PWR0167" H 4100 3400 50  0001 C CNN
F 1 "GND" H 4100 3500 50  0000 C CNN
F 2 "" H 4100 3650 50  0001 C CNN
F 3 "" H 4100 3650 50  0001 C CNN
	1    4100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3250 3950 3250
Wire Wire Line
	4100 3650 3950 3650
$Comp
L Motor:Stepper_Motor_bipolar M_Flip1
U 1 1 5EEEF77E
P 5200 3450
F 0 "M_Flip1" H 5400 3600 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 5400 3500 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B04B-EH-A_1x04_P2.50mm_Vertical" H 5210 3440 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 5210 3440 50  0001 C CNN
	1    5200 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3050 5300 3050
Wire Wire Line
	5300 3050 5300 3150
Wire Wire Line
	5100 3150 3950 3150
Wire Wire Line
	3950 3450 4900 3450
Wire Wire Line
	4900 3450 4900 3350
Wire Wire Line
	4900 3550 3950 3550
$Comp
L power:+24V #PWR0168
U 1 1 5EEF20E5
P 4900 800
F 0 "#PWR0168" H 4900 650 50  0001 C CNN
F 1 "+24V" H 4950 1000 50  0000 C CNN
F 2 "" H 4900 800 50  0001 C CNN
F 3 "" H 4900 800 50  0001 C CNN
	1    4900 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EEF30F1
P 4900 900
AR Path="/5E9C8987/5EEF30F1" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EEF30F1" Ref="C42"  Part="1" 
F 0 "C42" H 4992 946 50  0000 L CNN
F 1 "220uF" H 4992 855 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 900 50  0001 C CNN
F 3 "~" H 4900 900 50  0001 C CNN
	1    4900 900 
	1    0    0    -1  
$EndComp
Connection ~ 4900 800 
Wire Wire Line
	4900 800  5250 800 
Wire Wire Line
	5250 2250 4800 2250
Wire Wire Line
	5250 800  5250 2250
$Comp
L power:GND #PWR0169
U 1 1 5EEF6E4B
P 5250 2450
F 0 "#PWR0169" H 5250 2200 50  0001 C CNN
F 1 "GND" H 5250 2300 50  0000 C CNN
F 2 "" H 5250 2450 50  0001 C CNN
F 3 "" H 5250 2450 50  0001 C CNN
	1    5250 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EEF6EE7
P 5250 2350
AR Path="/5E9C8987/5EEF6EE7" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EEF6EE7" Ref="C43"  Part="1" 
F 0 "C43" H 5342 2396 50  0000 L CNN
F 1 "220nF" H 5342 2305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5250 2350 50  0001 C CNN
F 3 "~" H 5250 2350 50  0001 C CNN
	1    5250 2350
	1    0    0    -1  
$EndComp
Connection ~ 5250 2250
$Comp
L power:GND #PWR0170
U 1 1 5EEF8BF1
P 1850 2350
F 0 "#PWR0170" H 1850 2100 50  0001 C CNN
F 1 "GND" H 1900 2150 50  0000 C CNN
F 2 "" H 1850 2350 50  0001 C CNN
F 3 "" H 1850 2350 50  0001 C CNN
	1    1850 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EEF917F
P 1850 2200
AR Path="/5E9C8987/5EEF917F" Ref="R?"  Part="1" 
AR Path="/5EA6A304/5EEF917F" Ref="R21"  Part="1" 
F 0 "R21" V 2057 2200 50  0000 C CNN
F 1 "330" V 1966 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 2200 50  0001 C CNN
F 3 "~" H 1850 2200 50  0001 C CNN
	1    1850 2200
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5EEFB8C5
P 1850 1900
AR Path="/5E9C8987/5EEFB8C5" Ref="R?"  Part="1" 
AR Path="/5EA6A304/5EEFB8C5" Ref="R20"  Part="1" 
F 0 "R20" V 2057 1900 50  0000 C CNN
F 1 "330" V 1966 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1780 1900 50  0001 C CNN
F 3 "~" H 1850 1900 50  0001 C CNN
	1    1850 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	2150 2550 2150 2050
Wire Wire Line
	2150 2050 1850 2050
Connection ~ 1850 2050
Wire Wire Line
	1850 1750 1850 800 
Wire Wire Line
	1850 800  4900 800 
$Comp
L Device:C_Small C?
U 1 1 5EF01CFA
P 4300 2350
AR Path="/5E9C8987/5EF01CFA" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EF01CFA" Ref="CBoot1"  Part="1" 
F 0 "CBoot1" V 4250 2200 50  0000 C CNN
F 1 "470nF" V 4250 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4300 2350 50  0001 C CNN
F 3 "~" H 4300 2350 50  0001 C CNN
	1    4300 2350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4400 2350 4450 2350
Wire Wire Line
	4450 2350 4450 2250
Connection ~ 4450 2250
Wire Wire Line
	4450 2250 3950 2250
$Comp
L Device:D D6
U 1 1 5EF0E62D
P 4800 2450
F 0 "D6" V 4900 2450 50  0000 R CNN
F 1 "D" V 4800 2350 50  0000 R CNN
F 2 "S1G:DIOM5436X245N" H 4800 2450 50  0001 C CNN
F 3 "~" H 4800 2450 50  0001 C CNN
	1    4800 2450
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D7
U 1 1 5EF0E7B3
P 4800 2750
F 0 "D7" V 4900 2750 50  0000 R CNN
F 1 "D" V 4800 2650 50  0000 R CNN
F 2 "S1G:DIOM5436X245N" H 4800 2750 50  0001 C CNN
F 3 "~" H 4800 2750 50  0001 C CNN
	1    4800 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4800 2550 4800 2600
Wire Wire Line
	4300 2550 4800 2550
Connection ~ 4800 2600
Wire Wire Line
	4800 2300 4800 2250
Connection ~ 4800 2250
Wire Wire Line
	4800 2250 4450 2250
Wire Wire Line
	3950 2550 4100 2550
$Comp
L Device:C_Small CBoot?
U 1 1 5EF0881E
P 4200 2550
AR Path="/5E9C8987/5EF0881E" Ref="CBoot?"  Part="1" 
AR Path="/5EA6A304/5EF0881E" Ref="Cfly1"  Part="1" 
F 0 "Cfly1" V 4050 2550 50  0000 C CNN
F 1 "47nF" V 4150 2400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4200 2550 50  0001 C CNN
F 3 "~" H 4200 2550 50  0001 C CNN
	1    4200 2550
	0    -1   -1   0   
$EndComp
$Comp
L POWERSTEP01:POWERSTEP01 U10
U 1 1 5EE76F82
P 3050 3050
F 0 "U10" H 3050 4650 50  0000 C CNN
F 1 "POWERSTEP01" H 3050 4550 50  0000 C CNN
F 2 "PowerFP:IC_POWERSTEP01" H 3050 3050 50  0001 L BNN
F 3 "ST Microelectronics" H 3050 3050 50  0001 L BNN
F 4 "6" H 3050 3050 50  0001 L BNN "Field4"
F 5 "Manufacturer Recommendations" H 3050 3050 50  0001 L BNN "Field5"
	1    3050 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2350 4000 2350
Wire Wire Line
	4000 2350 4000 2900
Connection ~ 4000 2350
Wire Wire Line
	4000 2350 3950 2350
Wire Wire Line
	4000 2900 4800 2900
Wire Wire Line
	3950 2050 4450 2050
Wire Wire Line
	4450 2050 4450 2250
$Comp
L power:GND #PWR0171
U 1 1 5EF25E98
P 4900 2050
F 0 "#PWR0171" H 4900 1800 50  0001 C CNN
F 1 "GND" H 4900 1900 50  0000 C CNN
F 2 "" H 4900 2050 50  0001 C CNN
F 3 "" H 4900 2050 50  0001 C CNN
	1    4900 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF25E9E
P 4550 2050
AR Path="/5E9C8987/5EF25E9E" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EF25E9E" Ref="C41"  Part="1" 
F 0 "C41" V 4600 2100 50  0000 C CNN
F 1 "100nF" V 4600 1900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4550 2050 50  0001 C CNN
F 3 "~" H 4550 2050 50  0001 C CNN
	1    4550 2050
	0    -1   -1   0   
$EndComp
Connection ~ 4450 2050
Wire Wire Line
	3950 2150 4100 2150
Wire Wire Line
	4100 2150 4100 1750
Wire Wire Line
	4100 1750 3950 1750
$Comp
L Device:C_Small C?
U 1 1 5EF2F1DA
P 4450 1750
AR Path="/5E9C8987/5EF2F1DA" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EF2F1DA" Ref="C40"  Part="1" 
F 0 "C40" V 4500 1800 50  0000 C CNN
F 1 "470nF" V 4500 1600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4450 1750 50  0001 C CNN
F 3 "~" H 4450 1750 50  0001 C CNN
	1    4450 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF30C2B
P 4450 1550
AR Path="/5E9C8987/5EF30C2B" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EF30C2B" Ref="C39"  Part="1" 
F 0 "C39" V 4500 1600 50  0000 C CNN
F 1 "100nF" V 4500 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4450 1550 50  0001 C CNN
F 3 "~" H 4450 1550 50  0001 C CNN
	1    4450 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4900 1000 4900 1050
Wire Wire Line
	4550 1550 4900 1550
Connection ~ 4900 1550
Wire Wire Line
	4900 1550 4900 1750
Wire Wire Line
	4550 1750 4900 1750
Connection ~ 4900 1750
Wire Wire Line
	4900 1750 4900 2050
Wire Wire Line
	4650 2050 4900 2050
Connection ~ 4900 2050
Wire Wire Line
	4350 1550 4100 1550
Wire Wire Line
	4100 1550 4100 1750
Connection ~ 4100 1750
Wire Wire Line
	4350 1750 4100 1750
$Comp
L Device:C_Small C?
U 1 1 5EF4F04A
P 4450 1150
AR Path="/5E9C8987/5EF4F04A" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EF4F04A" Ref="C37"  Part="1" 
F 0 "C37" V 4500 1200 50  0000 C CNN
F 1 "100nF" V 4500 1000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4450 1150 50  0001 C CNN
F 3 "~" H 4450 1150 50  0001 C CNN
	1    4450 1150
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EF5121B
P 4450 1350
AR Path="/5E9C8987/5EF5121B" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EF5121B" Ref="C38"  Part="1" 
F 0 "C38" V 4500 1400 50  0000 C CNN
F 1 "100nF" V 4500 1200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4450 1350 50  0001 C CNN
F 3 "~" H 4450 1350 50  0001 C CNN
	1    4450 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 1950 4000 1950
Wire Wire Line
	4000 1950 4000 1850
Wire Wire Line
	4000 1150 4350 1150
Wire Wire Line
	3950 1850 4000 1850
Connection ~ 4000 1850
Wire Wire Line
	4000 1850 4000 1350
Wire Wire Line
	4000 1350 4350 1350
Connection ~ 4000 1350
Wire Wire Line
	4000 1350 4000 1150
$Comp
L Device:C_Small C?
U 1 1 5EF67559
P 4450 950
AR Path="/5E9C8987/5EF67559" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EF67559" Ref="C36"  Part="1" 
F 0 "C36" V 4500 1000 50  0000 C CNN
F 1 "22uF" V 4500 800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4450 950 50  0001 C CNN
F 3 "~" H 4450 950 50  0001 C CNN
	1    4450 950 
	0    -1   -1   0   
$EndComp
Connection ~ 4900 1350
Wire Wire Line
	4900 1350 4900 1550
Connection ~ 4900 1150
Wire Wire Line
	4900 1150 4900 1350
Wire Wire Line
	4550 1150 4900 1150
Wire Wire Line
	4550 1350 4900 1350
Wire Wire Line
	4550 950  4550 1050
Wire Wire Line
	4550 1050 4900 1050
Connection ~ 4900 1050
Wire Wire Line
	4900 1050 4900 1150
Wire Wire Line
	4000 950  4000 1150
Wire Wire Line
	4000 950  4350 950 
Connection ~ 4000 1150
Text Label 6500 2850 0    50   ~ 0
NSS1
Text Label 6500 2950 0    50   ~ 0
SCK
Text Label 6500 3050 0    50   ~ 0
MISO
Text Label 6500 3150 0    50   ~ 0
MOSI
Wire Wire Line
	6400 2850 7150 2850
Wire Wire Line
	6400 2950 7150 2950
Wire Wire Line
	6400 3050 7150 3050
Wire Wire Line
	6400 3150 7150 3150
$Comp
L power:GND #PWR0172
U 1 1 5EFA19D7
P 8950 4450
F 0 "#PWR0172" H 8950 4200 50  0001 C CNN
F 1 "GND" H 9000 4250 50  0000 C CNN
F 2 "" H 8950 4450 50  0001 C CNN
F 3 "" H 8950 4450 50  0001 C CNN
	1    8950 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0173
U 1 1 5EFA19DD
P 6850 3850
F 0 "#PWR0173" H 6850 3600 50  0001 C CNN
F 1 "GND" H 6900 3650 50  0000 C CNN
F 2 "" H 6850 3850 50  0001 C CNN
F 3 "" H 6850 3850 50  0001 C CNN
	1    6850 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3850 6850 3850
Wire Wire Line
	7150 3750 6850 3750
Wire Wire Line
	6850 3750 6850 3850
Connection ~ 6850 3850
$Comp
L power:GND #PWR0174
U 1 1 5EFA19E7
P 9100 3350
F 0 "#PWR0174" H 9100 3100 50  0001 C CNN
F 1 "GND" H 9100 3200 50  0000 C CNN
F 2 "" H 9100 3350 50  0001 C CNN
F 3 "" H 9100 3350 50  0001 C CNN
	1    9100 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0175
U 1 1 5EFA19ED
P 9100 3750
F 0 "#PWR0175" H 9100 3500 50  0001 C CNN
F 1 "GND" H 9100 3600 50  0000 C CNN
F 2 "" H 9100 3750 50  0001 C CNN
F 3 "" H 9100 3750 50  0001 C CNN
	1    9100 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 3350 8950 3350
Wire Wire Line
	9100 3750 8950 3750
$Comp
L Motor:Stepper_Motor_bipolar M_Flip2
U 1 1 5EFA19F5
P 10200 3550
F 0 "M_Flip2" H 10400 3700 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 10400 3600 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B04B-EH-A_1x04_P2.50mm_Vertical" H 10210 3540 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 10210 3540 50  0001 C CNN
	1    10200 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 3150 10300 3150
Wire Wire Line
	10300 3150 10300 3250
Wire Wire Line
	10100 3250 8950 3250
Wire Wire Line
	8950 3550 9900 3550
Wire Wire Line
	9900 3550 9900 3450
Wire Wire Line
	9900 3650 8950 3650
$Comp
L power:+24V #PWR0176
U 1 1 5EFA1A02
P 9900 900
F 0 "#PWR0176" H 9900 750 50  0001 C CNN
F 1 "+24V" H 9950 1100 50  0000 C CNN
F 2 "" H 9900 900 50  0001 C CNN
F 3 "" H 9900 900 50  0001 C CNN
	1    9900 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EFA1A08
P 9900 1000
AR Path="/5E9C8987/5EFA1A08" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EFA1A08" Ref="C50"  Part="1" 
F 0 "C50" H 9992 1046 50  0000 L CNN
F 1 "220uF" H 9992 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9900 1000 50  0001 C CNN
F 3 "~" H 9900 1000 50  0001 C CNN
	1    9900 1000
	1    0    0    -1  
$EndComp
Connection ~ 9900 900 
Wire Wire Line
	9900 900  10250 900 
Wire Wire Line
	10250 2350 9800 2350
Wire Wire Line
	10250 900  10250 2350
$Comp
L power:GND #PWR0177
U 1 1 5EFA1A13
P 10250 2550
F 0 "#PWR0177" H 10250 2300 50  0001 C CNN
F 1 "GND" H 10250 2400 50  0000 C CNN
F 2 "" H 10250 2550 50  0001 C CNN
F 3 "" H 10250 2550 50  0001 C CNN
	1    10250 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EFA1A19
P 10250 2450
AR Path="/5E9C8987/5EFA1A19" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EFA1A19" Ref="C51"  Part="1" 
F 0 "C51" H 10342 2496 50  0000 L CNN
F 1 "220nF" H 10342 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10250 2450 50  0001 C CNN
F 3 "~" H 10250 2450 50  0001 C CNN
	1    10250 2450
	1    0    0    -1  
$EndComp
Connection ~ 10250 2350
$Comp
L power:GND #PWR0178
U 1 1 5EFA1A21
P 6850 2450
F 0 "#PWR0178" H 6850 2200 50  0001 C CNN
F 1 "GND" H 6900 2250 50  0000 C CNN
F 2 "" H 6850 2450 50  0001 C CNN
F 3 "" H 6850 2450 50  0001 C CNN
	1    6850 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EFA1A27
P 6850 2300
AR Path="/5E9C8987/5EFA1A27" Ref="R?"  Part="1" 
AR Path="/5EA6A304/5EFA1A27" Ref="R23"  Part="1" 
F 0 "R23" V 7057 2300 50  0000 C CNN
F 1 "330" V 6966 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6780 2300 50  0001 C CNN
F 3 "~" H 6850 2300 50  0001 C CNN
	1    6850 2300
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5EFA1A2E
P 6850 2000
AR Path="/5E9C8987/5EFA1A2E" Ref="R?"  Part="1" 
AR Path="/5EA6A304/5EFA1A2E" Ref="R22"  Part="1" 
F 0 "R22" V 7057 2000 50  0000 C CNN
F 1 "330" V 6966 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6780 2000 50  0001 C CNN
F 3 "~" H 6850 2000 50  0001 C CNN
	1    6850 2000
	-1   0    0    1   
$EndComp
Wire Wire Line
	7150 2650 7150 2150
Wire Wire Line
	7150 2150 6850 2150
Connection ~ 6850 2150
Wire Wire Line
	6850 1850 6850 900 
Wire Wire Line
	6850 900  9900 900 
$Comp
L Device:C_Small CBoot?
U 1 1 5EFA1A3A
P 9300 2450
AR Path="/5E9C8987/5EFA1A3A" Ref="CBoot?"  Part="1" 
AR Path="/5EA6A304/5EFA1A3A" Ref="CBoot2"  Part="1" 
F 0 "CBoot2" V 9250 2300 50  0000 C CNN
F 1 "470nF" V 9250 2600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9300 2450 50  0001 C CNN
F 3 "~" H 9300 2450 50  0001 C CNN
	1    9300 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9400 2450 9450 2450
Wire Wire Line
	9450 2450 9450 2350
Connection ~ 9450 2350
Wire Wire Line
	9450 2350 8950 2350
$Comp
L Device:D D8
U 1 1 5EFA1A45
P 9800 2550
F 0 "D8" V 9900 2550 50  0000 R CNN
F 1 "D" V 9800 2450 50  0000 R CNN
F 2 "S1G:DIOM5436X245N" H 9800 2550 50  0001 C CNN
F 3 "~" H 9800 2550 50  0001 C CNN
	1    9800 2550
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D9
U 1 1 5EFA1A4C
P 9800 2850
F 0 "D9" V 9900 2850 50  0000 R CNN
F 1 "D" V 9800 2750 50  0000 R CNN
F 2 "S1G:DIOM5436X245N" H 9800 2850 50  0001 C CNN
F 3 "~" H 9800 2850 50  0001 C CNN
	1    9800 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9800 2650 9800 2700
Wire Wire Line
	9300 2650 9800 2650
Connection ~ 9800 2700
Wire Wire Line
	9800 2400 9800 2350
Connection ~ 9800 2350
Wire Wire Line
	9800 2350 9450 2350
Wire Wire Line
	8950 2650 9100 2650
$Comp
L Device:C_Small Cfly?
U 1 1 5EFA1A5A
P 9200 2650
AR Path="/5E9C8987/5EFA1A5A" Ref="Cfly?"  Part="1" 
AR Path="/5EA6A304/5EFA1A5A" Ref="Cfly2"  Part="1" 
F 0 "Cfly2" V 9050 2650 50  0000 C CNN
F 1 "47nF" V 9150 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9200 2650 50  0001 C CNN
F 3 "~" H 9200 2650 50  0001 C CNN
	1    9200 2650
	0    -1   -1   0   
$EndComp
$Comp
L POWERSTEP01:POWERSTEP01 U11
U 1 1 5EFA1A63
P 8050 3150
F 0 "U11" H 8050 4750 50  0000 C CNN
F 1 "POWERSTEP01" H 8050 4650 50  0000 C CNN
F 2 "PowerFP:IC_POWERSTEP01" H 8050 3150 50  0001 L BNN
F 3 "ST Microelectronics" H 8050 3150 50  0001 L BNN
F 4 "6" H 8050 3150 50  0001 L BNN "Field4"
F 5 "Manufacturer Recommendations" H 8050 3150 50  0001 L BNN "Field5"
	1    8050 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 2450 9000 2450
Wire Wire Line
	9000 2450 9000 3000
Connection ~ 9000 2450
Wire Wire Line
	9000 2450 8950 2450
Wire Wire Line
	9000 3000 9800 3000
Wire Wire Line
	8950 2150 9450 2150
Wire Wire Line
	9450 2150 9450 2350
$Comp
L power:GND #PWR0179
U 1 1 5EFA1A71
P 9900 2150
F 0 "#PWR0179" H 9900 1900 50  0001 C CNN
F 1 "GND" H 9900 2000 50  0000 C CNN
F 2 "" H 9900 2150 50  0001 C CNN
F 3 "" H 9900 2150 50  0001 C CNN
	1    9900 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EFA1A77
P 9550 2150
AR Path="/5E9C8987/5EFA1A77" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EFA1A77" Ref="C49"  Part="1" 
F 0 "C49" V 9600 2200 50  0000 C CNN
F 1 "100nF" V 9600 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9550 2150 50  0001 C CNN
F 3 "~" H 9550 2150 50  0001 C CNN
	1    9550 2150
	0    -1   -1   0   
$EndComp
Connection ~ 9450 2150
Wire Wire Line
	8950 2250 9100 2250
Wire Wire Line
	9100 2250 9100 1850
Wire Wire Line
	9100 1850 8950 1850
$Comp
L Device:C_Small C?
U 1 1 5EFA1A82
P 9450 1850
AR Path="/5E9C8987/5EFA1A82" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EFA1A82" Ref="C48"  Part="1" 
F 0 "C48" V 9500 1900 50  0000 C CNN
F 1 "470nF" V 9500 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9450 1850 50  0001 C CNN
F 3 "~" H 9450 1850 50  0001 C CNN
	1    9450 1850
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EFA1A89
P 9450 1650
AR Path="/5E9C8987/5EFA1A89" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EFA1A89" Ref="C47"  Part="1" 
F 0 "C47" V 9500 1700 50  0000 C CNN
F 1 "100nF" V 9500 1500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9450 1650 50  0001 C CNN
F 3 "~" H 9450 1650 50  0001 C CNN
	1    9450 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9900 1100 9900 1150
Wire Wire Line
	9550 1650 9900 1650
Connection ~ 9900 1650
Wire Wire Line
	9900 1650 9900 1850
Wire Wire Line
	9550 1850 9900 1850
Connection ~ 9900 1850
Wire Wire Line
	9900 1850 9900 2150
Wire Wire Line
	9650 2150 9900 2150
Connection ~ 9900 2150
Wire Wire Line
	9350 1650 9100 1650
Wire Wire Line
	9100 1650 9100 1850
Connection ~ 9100 1850
Wire Wire Line
	9350 1850 9100 1850
$Comp
L Device:C_Small C?
U 1 1 5EFA1A9D
P 9450 1250
AR Path="/5E9C8987/5EFA1A9D" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EFA1A9D" Ref="C45"  Part="1" 
F 0 "C45" V 9500 1300 50  0000 C CNN
F 1 "100nF" V 9500 1100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9450 1250 50  0001 C CNN
F 3 "~" H 9450 1250 50  0001 C CNN
	1    9450 1250
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EFA1AA4
P 9450 1450
AR Path="/5E9C8987/5EFA1AA4" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EFA1AA4" Ref="C46"  Part="1" 
F 0 "C46" V 9500 1500 50  0000 C CNN
F 1 "100nF" V 9500 1300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9450 1450 50  0001 C CNN
F 3 "~" H 9450 1450 50  0001 C CNN
	1    9450 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 2050 9000 2050
Wire Wire Line
	9000 2050 9000 1950
Wire Wire Line
	9000 1250 9350 1250
Wire Wire Line
	8950 1950 9000 1950
Connection ~ 9000 1950
Wire Wire Line
	9000 1950 9000 1450
Wire Wire Line
	9000 1450 9350 1450
Connection ~ 9000 1450
Wire Wire Line
	9000 1450 9000 1250
$Comp
L Device:C_Small C?
U 1 1 5EFA1AB4
P 9450 1050
AR Path="/5E9C8987/5EFA1AB4" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EFA1AB4" Ref="C44"  Part="1" 
F 0 "C44" V 9500 1100 50  0000 C CNN
F 1 "22uF" V 9500 900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9450 1050 50  0001 C CNN
F 3 "~" H 9450 1050 50  0001 C CNN
	1    9450 1050
	0    -1   -1   0   
$EndComp
Connection ~ 9900 1450
Wire Wire Line
	9900 1450 9900 1650
Connection ~ 9900 1250
Wire Wire Line
	9900 1250 9900 1450
Wire Wire Line
	9550 1250 9900 1250
Wire Wire Line
	9550 1450 9900 1450
Wire Wire Line
	9550 1050 9550 1150
Wire Wire Line
	9550 1150 9900 1150
Connection ~ 9900 1150
Wire Wire Line
	9900 1150 9900 1250
Wire Wire Line
	9000 1050 9000 1250
Wire Wire Line
	9000 1050 9350 1050
Connection ~ 9000 1250
$Comp
L B4B-EH-A_LF__SN_:B4B-EH-A_LF__SN_ J?
U 1 1 5EE51708
P 5650 5850
AR Path="/5E9C8987/5EE51708" Ref="J?"  Part="1" 
AR Path="/5EA6A304/5EE51708" Ref="J4"  Part="1" 
F 0 "J4" H 6100 5700 50  0000 L CNN
F 1 "Conn_GMotor1" H 5600 5400 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B04B-EH-A_1x04_P2.50mm_Vertical" H 6300 5950 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/1363/0900766b8136354a.pdf" H 6300 5850 50  0001 L CNN
F 4 "JST (JAPAN SOLDERLESS TERMINALS) - B4B-EH-A(LF)(SN) - CONNECTOR, HEADER, THT, 2.5MM, 4WAY" H 6300 5750 50  0001 L CNN "Description"
F 5 "6" H 6300 5650 50  0001 L CNN "Height"
F 6 "JST (JAPAN SOLDERLESS TERMINALS)" H 6300 5550 50  0001 L CNN "Manufacturer_Name"
F 7 "B4B-EH-A(LF)(SN)" H 6300 5450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 6300 5350 50  0001 L CNN "Mouser Part Number"
F 9 "" H 6300 5250 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6300 5150 50  0001 L CNN "RS Part Number"
F 11 "" H 6300 5050 50  0001 L CNN "RS Price/Stock"
	1    5650 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 5550 5650 5850
Wire Wire Line
	5400 5950 5650 5950
$Comp
L B4B-EH-A_LF__SN_:B4B-EH-A_LF__SN_ J?
U 1 1 5EE56ACB
P 5650 6850
AR Path="/5E9C8987/5EE56ACB" Ref="J?"  Part="1" 
AR Path="/5EA6A304/5EE56ACB" Ref="J5"  Part="1" 
F 0 "J5" H 6100 6700 50  0000 L CNN
F 1 "Conn_GMotor2" H 5600 6400 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B04B-EH-A_1x04_P2.50mm_Vertical" H 6300 6950 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/1363/0900766b8136354a.pdf" H 6300 6850 50  0001 L CNN
F 4 "JST (JAPAN SOLDERLESS TERMINALS) - B4B-EH-A(LF)(SN) - CONNECTOR, HEADER, THT, 2.5MM, 4WAY" H 6300 6750 50  0001 L CNN "Description"
F 5 "6" H 6300 6650 50  0001 L CNN "Height"
F 6 "JST (JAPAN SOLDERLESS TERMINALS)" H 6300 6550 50  0001 L CNN "Manufacturer_Name"
F 7 "B4B-EH-A(LF)(SN)" H 6300 6450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 6300 6350 50  0001 L CNN "Mouser Part Number"
F 9 "" H 6300 6250 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6300 6150 50  0001 L CNN "RS Part Number"
F 11 "" H 6300 6050 50  0001 L CNN "RS Price/Stock"
	1    5650 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 6550 5650 6550
Wire Wire Line
	5650 6550 5650 6850
Wire Wire Line
	5400 6950 5650 6950
Text HLabel 1150 6350 0    50   BiDi ~ 0
SPID[0..4]
Entry Wire Line
	1300 6350 1400 6450
Entry Wire Line
	1300 6550 1400 6650
Entry Wire Line
	1300 6650 1400 6750
Entry Wire Line
	1300 6750 1400 6850
Wire Bus Line
	1300 6350 1150 6350
Text Label 1400 6650 0    50   ~ 0
SPID2
Text Label 1400 6750 0    50   ~ 0
SPID3
Text Label 1400 6850 0    50   ~ 0
SPID4
Wire Wire Line
	1400 6650 1850 6650
Wire Wire Line
	1400 6750 1850 6750
Wire Wire Line
	1400 6850 1850 6850
Text Label 1400 6550 0    50   ~ 0
SPID1
Wire Wire Line
	1400 6550 1850 6550
Text Label 1400 6450 0    50   ~ 0
SPID0
Wire Wire Line
	1400 6450 1850 6450
Entry Wire Line
	1300 6450 1400 6550
Wire Wire Line
	1400 3050 2150 3050
Wire Wire Line
	1400 2950 2150 2950
Wire Wire Line
	1400 2850 2150 2850
Wire Wire Line
	1400 2750 2150 2750
$Comp
L power:+24V #PWR0163
U 1 1 5EF46C80
P 2700 5650
F 0 "#PWR0163" H 2700 5500 50  0001 C CNN
F 1 "+24V" H 2750 5850 50  0000 C CNN
F 2 "" H 2700 5650 50  0001 C CNN
F 3 "" H 2700 5650 50  0001 C CNN
	1    2700 5650
	1    0    0    -1  
$EndComp
Text GLabel 5400 5650 0    50   Input ~ 0
+24V_O
Text GLabel 5400 6650 0    50   Input ~ 0
+24V_O
Text Label 1700 6450 0    50   ~ 0
NSS1
Text Label 1700 6550 0    50   ~ 0
NSS0
Text Label 1700 6650 0    50   ~ 0
SCK
Text Label 1700 6750 0    50   ~ 0
MISO
Text Label 1700 6850 0    50   ~ 0
MOSI
Text HLabel 1200 5450 0    50   BiDi ~ 0
PWM[0..3]
Entry Wire Line
	1350 5450 1450 5550
Entry Wire Line
	1350 5550 1450 5650
Entry Wire Line
	1350 5650 1450 5750
Entry Wire Line
	1350 5750 1450 5850
Wire Bus Line
	1350 5450 1200 5450
Text Label 1700 5550 0    50   ~ 0
PWM0
Text Label 1700 5650 0    50   ~ 0
PWM1
Text Label 1700 5750 0    50   ~ 0
PWM2
Text Label 1700 5850 0    50   ~ 0
PWM3
Wire Wire Line
	1450 5550 1900 5550
Wire Wire Line
	1450 5650 1900 5650
Wire Wire Line
	1450 5750 1900 5750
Wire Wire Line
	1450 5850 1900 5850
Text Label 5200 6050 0    50   ~ 0
PWM3
Wire Wire Line
	5200 6050 5650 6050
Wire Wire Line
	5400 5950 5400 5650
Text Label 5200 7050 0    50   ~ 0
PWM2
Wire Wire Line
	5200 7050 5650 7050
Wire Wire Line
	5400 6950 5400 6650
Text Label 3000 5550 0    50   ~ 0
PWM0
Wire Wire Line
	3000 5550 3350 5550
Wire Wire Line
	2700 6950 3350 6950
Text Label 2900 6850 0    50   ~ 0
PWM1
Wire Wire Line
	2900 6850 3350 6850
Text Label 5350 5400 0    50   ~ 0
GoldenMotors
Wire Wire Line
	2700 5650 3350 5650
$Comp
L power:+24V #PWR0187
U 1 1 5EFDA63D
P 2700 6950
F 0 "#PWR0187" H 2700 6800 50  0001 C CNN
F 1 "+24V" H 2750 7150 50  0000 C CNN
F 2 "" H 2700 6950 50  0001 C CNN
F 3 "" H 2700 6950 50  0001 C CNN
	1    2700 6950
	1    0    0    -1  
$EndComp
NoConn ~ 5650 6150
NoConn ~ 5650 7150
NoConn ~ 2150 4050
NoConn ~ 2150 3950
NoConn ~ 2150 3450
NoConn ~ 2150 3350
NoConn ~ 2150 3150
NoConn ~ 3950 3850
NoConn ~ 3950 3950
NoConn ~ 7150 3250
NoConn ~ 7150 3450
NoConn ~ 7150 3550
NoConn ~ 7150 4050
NoConn ~ 7150 4150
NoConn ~ 8950 4050
NoConn ~ 8950 3950
$Comp
L power:GNDS #PWR0133
U 1 1 5EEFB8C9
P 5550 5550
F 0 "#PWR0133" H 5550 5300 50  0001 C CNN
F 1 "GNDS" H 5600 5350 50  0000 C CNN
F 2 "" H 5550 5550 50  0001 C CNN
F 3 "" H 5550 5550 50  0001 C CNN
	1    5550 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 5550 5550 5550
$Comp
L power:GNDS #PWR0162
U 1 1 5EF177F6
P 5750 6550
F 0 "#PWR0162" H 5750 6300 50  0001 C CNN
F 1 "GNDS" H 5800 6350 50  0000 C CNN
F 2 "" H 5750 6550 50  0001 C CNN
F 3 "" H 5750 6550 50  0001 C CNN
	1    5750 6550
	1    0    0    -1  
$EndComp
Wire Bus Line
	1350 5450 1350 5750
Wire Bus Line
	1300 6350 1300 6750
$Comp
L power:GND #PWR?
U 1 1 5EF45AE8
P 4050 7200
F 0 "#PWR?" H 4050 6950 50  0001 C CNN
F 1 "GND" H 4100 7000 50  0000 C CNN
F 2 "" H 4050 7200 50  0001 C CNN
F 3 "" H 4050 7200 50  0001 C CNN
	1    4050 7200
	1    0    0    -1  
$EndComp
Connection ~ 4050 7200
$EndSCHEMATC
