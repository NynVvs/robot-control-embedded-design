EESchema Schematic File Version 4
LIBS:RobotIM-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Switching:CRE1S2405SC U13
U 1 1 5EA7528D
P 5250 3500
F 0 "U13" H 5250 3967 50  0000 C CNN
F 1 "CRE1S2405SC" H 5250 3876 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_muRata_CRE1xxxxxxSC_THT" H 5250 3100 50  0001 C CNN
F 3 "http://power.murata.com/datasheet?/data/power/ncl/kdc_cre1.pdf" H 5250 3000 50  0001 C CNN
	1    5250 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3300 5850 3300
$Comp
L power:GND #PWR0180
U 1 1 5EA756DE
P 6500 3650
F 0 "#PWR0180" H 6500 3400 50  0001 C CNN
F 1 "GND" H 6505 3477 50  0000 C CNN
F 2 "" H 6500 3650 50  0001 C CNN
F 3 "" H 6500 3650 50  0001 C CNN
	1    6500 3650
	1    0    0    -1  
$EndComp
Text GLabel 5750 2900 0    50   Input ~ 0
+5V
Text GLabel 7000 2900 0    50   Input ~ 0
+3V3
Wire Wire Line
	5750 2900 5850 2900
Wire Wire Line
	5850 2900 5850 3300
Connection ~ 5850 3300
Wire Wire Line
	5850 3300 6200 3300
$Comp
L Regulator_Linear:AMS1117-3.3 U15
U 1 1 5EA764FA
P 6500 3300
F 0 "U15" H 6500 3542 50  0000 C CNN
F 1 "AMS1117-3.3" H 6500 3451 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 6500 3500 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 6600 3050 50  0001 C CNN
	1    6500 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3300 7150 3300
Wire Wire Line
	7000 2900 7150 2900
Wire Wire Line
	7150 2900 7150 3300
Text GLabel 2250 4800 2    50   Input ~ 0
+42V
Text GLabel 4400 5100 2    50   Input ~ 0
+24V_O
Text GLabel 7600 3300 0    50   Input ~ 0
+5V
Text GLabel 9400 2900 0    50   Input ~ 0
+1V2
Wire Wire Line
	9400 2900 9400 3300
Wire Wire Line
	9400 3300 9050 3300
$Comp
L 39-28-1023:39-28-1023 JMDin1
U 1 1 5EE1DBDB
P 9900 1700
F 0 "JMDin1" H 10200 1300 50  0000 L CNN
F 1 "MOLEX 39-28-1023" H 10200 1400 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B02B-EH-A_1x02_P2.50mm_Vertical" H 10550 1800 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/4841883" H 10550 1700 50  0001 L CNN
F 4 "2 way vertical PCB header,Mini-Fit Jr Molex MINI-FIT JR. Series, Series Number 5566, 4.2mm Pitch 2 Way 2 Row Straight PCB Header, Solder Termination, 9A" H 10550 1600 50  0001 L CNN "Description"
F 5 "13.1" H 10550 1500 50  0001 L CNN "Height"
F 6 "Molex" H 10550 1400 50  0001 L CNN "Manufacturer_Name"
F 7 "39-28-1023" H 10550 1300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "538-39-28-1023" H 10550 1200 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Molex/39-28-1023?qs=LQQHZ4xw2XBykFU0jRHvlQ%3D%3D" H 10550 1100 50  0001 L CNN "Mouser Price/Stock"
F 10 "1888713" H 10550 1000 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1888713" H 10550 900 50  0001 L CNN "RS Price/Stock"
F 12 "70190665" H 10550 800 50  0001 L CNN "Allied_Number"
F 13 "https://www.alliedelec.com/molex-incorporated-39-28-1023/70190665/" H 10550 700 50  0001 L CNN "Allied Price/Stock"
	1    9900 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0190
U 1 1 5EE1E2D0
P 10050 1250
F 0 "#PWR0190" H 10050 1000 50  0001 C CNN
F 1 "GND" H 10055 1077 50  0000 C CNN
F 2 "" H 10050 1250 50  0001 C CNN
F 3 "" H 10050 1250 50  0001 C CNN
	1    10050 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 1250 9900 1250
Wire Wire Line
	9900 1250 9900 1700
$Comp
L 39-28-1023:39-28-1023 Bat33V1
U 1 1 5EE38BF0
P 1850 3600
F 0 "Bat33V1" H 2500 3600 50  0000 L CNN
F 1 "MOLEX 39-28-1023" H 1950 3300 50  0000 L CNN
F 2 "39-28-1023:SHDR2W120P550X420_2X1_540X960X1310P" H 2500 3700 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/4841883" H 2500 3600 50  0001 L CNN
F 4 "2 way vertical PCB header,Mini-Fit Jr Molex MINI-FIT JR. Series, Series Number 5566, 4.2mm Pitch 2 Way 2 Row Straight PCB Header, Solder Termination, 9A" H 2500 3500 50  0001 L CNN "Description"
F 5 "13.1" H 2500 3400 50  0001 L CNN "Height"
F 6 "Molex" H 2500 3300 50  0001 L CNN "Manufacturer_Name"
F 7 "39-28-1023" H 2500 3200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "538-39-28-1023" H 2500 3100 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Molex/39-28-1023?qs=LQQHZ4xw2XBykFU0jRHvlQ%3D%3D" H 2500 3000 50  0001 L CNN "Mouser Price/Stock"
F 10 "1888713" H 2500 2900 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1888713" H 2500 2800 50  0001 L CNN "RS Price/Stock"
F 12 "70190665" H 2500 2700 50  0001 L CNN "Allied_Number"
F 13 "https://www.alliedelec.com/molex-incorporated-39-28-1023/70190665/" H 2500 2600 50  0001 L CNN "Allied Price/Stock"
	1    1850 3600
	-1   0    0    1   
$EndComp
$Comp
L 39-28-1023:39-28-1023 Bat42V1
U 1 1 5EE3D041
P 1900 5200
F 0 "Bat42V1" H 2550 5200 50  0000 L CNN
F 1 "MOLEX 39-28-1023" H 2000 4900 50  0000 L CNN
F 2 "39-28-1023:SHDR2W120P550X420_2X1_540X960X1310P" H 2550 5300 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/4841883" H 2550 5200 50  0001 L CNN
F 4 "2 way vertical PCB header,Mini-Fit Jr Molex MINI-FIT JR. Series, Series Number 5566, 4.2mm Pitch 2 Way 2 Row Straight PCB Header, Solder Termination, 9A" H 2550 5100 50  0001 L CNN "Description"
F 5 "13.1" H 2550 5000 50  0001 L CNN "Height"
F 6 "Molex" H 2550 4900 50  0001 L CNN "Manufacturer_Name"
F 7 "39-28-1023" H 2550 4800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "538-39-28-1023" H 2550 4700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Molex/39-28-1023?qs=LQQHZ4xw2XBykFU0jRHvlQ%3D%3D" H 2550 4600 50  0001 L CNN "Mouser Price/Stock"
F 10 "1888713" H 2550 4500 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1888713" H 2550 4400 50  0001 L CNN "RS Price/Stock"
F 12 "70190665" H 2550 4300 50  0001 L CNN "Allied_Number"
F 13 "https://www.alliedelec.com/molex-incorporated-39-28-1023/70190665/" H 2550 4200 50  0001 L CNN "Allied Price/Stock"
	1    1900 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1900 5100 2250 5100
Wire Wire Line
	1900 5200 2250 5200
Wire Wire Line
	9600 1800 9900 1800
Wire Wire Line
	9600 1350 9600 1800
$Comp
L power:+24V #PWR0191
U 1 1 5EE1DDF3
P 9600 1350
F 0 "#PWR0191" H 9600 1200 50  0001 C CNN
F 1 "+24V" H 9650 1550 50  0000 C CNN
F 2 "" H 9600 1350 50  0001 C CNN
F 3 "" H 9600 1350 50  0001 C CNN
	1    9600 1350
	1    0    0    -1  
$EndComp
Text GLabel 4150 2200 2    50   Input ~ 0
+24V
$Comp
L power:GND #PWR0132
U 1 1 5ECC4116
P 7900 3550
F 0 "#PWR0132" H 7900 3300 50  0001 C CNN
F 1 "GND" H 7950 3350 50  0000 C CNN
F 2 "" H 7900 3550 50  0001 C CNN
F 3 "" H 7900 3550 50  0001 C CNN
	1    7900 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3600 6500 3650
Wire Wire Line
	5750 3700 6500 3700
Connection ~ 6500 3650
Wire Wire Line
	6500 3650 6500 3700
Wire Wire Line
	7900 3500 7900 3550
$Comp
L power:GND #PWR0192
U 1 1 5ECC636A
P 5600 2450
F 0 "#PWR0192" H 5600 2200 50  0001 C CNN
F 1 "GND" H 5650 2250 50  0000 C CNN
F 2 "" H 5600 2450 50  0001 C CNN
F 3 "" H 5600 2450 50  0001 C CNN
	1    5600 2450
	1    0    0    -1  
$EndComp
$Comp
L 39-28-1023:39-28-1023 JMdyn2
U 1 1 5EDA5811
P 8450 1750
F 0 "JMdyn2" H 8750 1350 50  0000 L CNN
F 1 "MOLEX 39-28-1023" H 8750 1450 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B02B-EH-A_1x02_P2.50mm_Vertical" H 9100 1850 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/4841883" H 9100 1750 50  0001 L CNN
F 4 "2 way vertical PCB header,Mini-Fit Jr Molex MINI-FIT JR. Series, Series Number 5566, 4.2mm Pitch 2 Way 2 Row Straight PCB Header, Solder Termination, 9A" H 9100 1650 50  0001 L CNN "Description"
F 5 "13.1" H 9100 1550 50  0001 L CNN "Height"
F 6 "Molex" H 9100 1450 50  0001 L CNN "Manufacturer_Name"
F 7 "39-28-1023" H 9100 1350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "538-39-28-1023" H 9100 1250 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Molex/39-28-1023?qs=LQQHZ4xw2XBykFU0jRHvlQ%3D%3D" H 9100 1150 50  0001 L CNN "Mouser Price/Stock"
F 10 "1888713" H 9100 1050 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1888713" H 9100 950 50  0001 L CNN "RS Price/Stock"
F 12 "70190665" H 9100 850 50  0001 L CNN "Allied_Number"
F 13 "https://www.alliedelec.com/molex-incorporated-39-28-1023/70190665/" H 9100 750 50  0001 L CNN "Allied Price/Stock"
	1    8450 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0194
U 1 1 5EDA5818
P 8600 1300
F 0 "#PWR0194" H 8600 1050 50  0001 C CNN
F 1 "GND" H 8605 1127 50  0000 C CNN
F 2 "" H 8600 1300 50  0001 C CNN
F 3 "" H 8600 1300 50  0001 C CNN
	1    8600 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 1300 8450 1300
Wire Wire Line
	8450 1300 8450 1750
Wire Wire Line
	8150 1850 8450 1850
Wire Wire Line
	8150 1400 8150 1850
$Comp
L power:+24V #PWR0195
U 1 1 5EDA5822
P 8150 1400
F 0 "#PWR0195" H 8150 1250 50  0001 C CNN
F 1 "+24V" H 8200 1600 50  0000 C CNN
F 2 "" H 8150 1400 50  0001 C CNN
F 3 "" H 8150 1400 50  0001 C CNN
	1    8150 1400
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:CRE1S2412SC U17
U 1 1 5ECFF498
P 6500 2250
F 0 "U17" H 6500 2750 50  0000 C CNN
F 1 "CRE1S2412SC" H 6500 2650 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_muRata_CRE1xxxxxxSC_THT" H 6500 1850 50  0001 C CNN
F 3 "http://power.murata.com/datasheet?/data/power/ncl/kdc_cre1.pdf" H 6500 1750 50  0001 C CNN
	1    6500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2450 5600 2450
Wire Wire Line
	7000 2050 7150 2050
Wire Wire Line
	7150 2050 7150 1500
$Comp
L power:GND #PWR0200
U 1 1 5ED01468
P 7150 2450
F 0 "#PWR0200" H 7150 2200 50  0001 C CNN
F 1 "GND" H 7200 2250 50  0000 C CNN
F 2 "" H 7150 2450 50  0001 C CNN
F 3 "" H 7150 2450 50  0001 C CNN
	1    7150 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2450 7150 2450
$Comp
L Regulator_Switching:LM2842Y U18
U 1 1 5EE329A0
P 3000 5200
F 0 "U18" H 3000 5600 50  0000 C CNN
F 1 "LM2842Y" H 3000 5500 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-6" H 3025 4950 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2842-q1.pdf" H 2900 5300 50  0001 C CNN
	1    3000 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 5300 2600 5100
Connection ~ 2600 5100
Wire Wire Line
	2600 5100 2400 5100
Wire Wire Line
	2250 5200 2250 5500
Connection ~ 2250 5100
Wire Wire Line
	2250 4800 2250 5100
Wire Wire Line
	3000 5500 2400 5500
$Comp
L power:+12V #PWR0189
U 1 1 5EE3C26D
P 7150 1500
F 0 "#PWR0189" H 7150 1350 50  0001 C CNN
F 1 "+12V" H 7200 1700 50  0000 C CNN
F 2 "" H 7150 1500 50  0001 C CNN
F 3 "" H 7150 1500 50  0001 C CNN
	1    7150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3300 4550 3300
Wire Wire Line
	6000 2050 4550 2050
Wire Wire Line
	4550 2050 4550 3300
Connection ~ 4550 3300
Wire Wire Line
	4550 3300 4150 3300
Wire Wire Line
	4150 2200 4150 3300
$Comp
L bu12javg:BU12JA2VG U14
U 1 1 5EE30DD1
P 8600 2950
F 0 "U14" H 8650 2900 50  0000 C CNN
F 1 "BU12JA2VG" H 8650 2800 50  0000 C CNN
F 2 "regulator5vto1:SOT95P280X125-5N" H 8600 2950 50  0001 C CNN
F 3 "" H 8600 2950 50  0001 C CNN
	1    8600 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 3300 8150 3300
$Comp
L Device:C_Small C?
U 1 1 5EE43E6C
P 7900 3400
AR Path="/5E9C8987/5EE43E6C" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EE43E6C" Ref="C?"  Part="1" 
AR Path="/5EA6B564/5EE43E6C" Ref="C52"  Part="1" 
F 0 "C52" V 7950 3450 50  0000 C CNN
F 1 "470nF" V 7950 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7900 3400 50  0001 C CNN
F 3 "~" H 7900 3400 50  0001 C CNN
	1    7900 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EE44C22
P 9400 3400
AR Path="/5E9C8987/5EE44C22" Ref="C?"  Part="1" 
AR Path="/5EA6A304/5EE44C22" Ref="C?"  Part="1" 
AR Path="/5EA6B564/5EE44C22" Ref="C53"  Part="1" 
F 0 "C53" V 9450 3450 50  0000 C CNN
F 1 "470nF" V 9450 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9400 3400 50  0001 C CNN
F 3 "~" H 9400 3400 50  0001 C CNN
	1    9400 3400
	-1   0    0    1   
$EndComp
Connection ~ 7900 3300
Wire Wire Line
	7900 3300 7600 3300
Connection ~ 9400 3300
$Comp
L power:GND #PWR0181
U 1 1 5EE4ACF5
P 9400 3550
F 0 "#PWR0181" H 9400 3300 50  0001 C CNN
F 1 "GND" H 9450 3350 50  0000 C CNN
F 2 "" H 9400 3550 50  0001 C CNN
F 3 "" H 9400 3550 50  0001 C CNN
	1    9400 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 3500 9400 3550
Wire Wire Line
	9050 3400 9250 3400
Wire Wire Line
	9250 3400 9250 3550
Wire Wire Line
	9250 3550 9400 3550
Connection ~ 9400 3550
Wire Wire Line
	9050 3500 9050 3750
Wire Wire Line
	9050 3750 8150 3750
Wire Wire Line
	8150 3750 8150 3300
Connection ~ 8150 3300
Wire Wire Line
	8150 3300 7900 3300
$Comp
L Device:C_Small C?
U 1 1 5EE90E17
P 2400 5300
AR Path="/5E9C8987/5EE90E17" Ref="C?"  Part="1" 
AR Path="/5EA6B564/5EE90E17" Ref="C55"  Part="1" 
F 0 "C55" H 2492 5346 50  0000 L CNN
F 1 "2.2uF" H 2492 5255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2400 5300 50  0001 C CNN
F 3 "~" H 2400 5300 50  0001 C CNN
	1    2400 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5200 2400 5100
Connection ~ 2400 5100
Wire Wire Line
	2400 5100 2250 5100
Wire Wire Line
	2400 5400 2400 5500
Connection ~ 2400 5500
Wire Wire Line
	2400 5500 2250 5500
$Comp
L Device:R R25
U 1 1 5EE9AF06
P 3450 5600
F 0 "R25" H 3550 5650 50  0000 L CNN
F 1 "1K" H 3550 5600 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3380 5600 50  0001 C CNN
F 3 "~" H 3450 5600 50  0001 C CNN
	1    3450 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 5300 3450 5300
Wire Wire Line
	3450 5300 3450 5450
Wire Wire Line
	3450 5750 2250 5750
Wire Wire Line
	2250 5750 2250 5500
Connection ~ 2250 5500
$Comp
L Device:R R24
U 1 1 5EEA18D4
P 3700 5450
F 0 "R24" V 3800 5450 50  0000 C CNN
F 1 "30K" V 3700 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3630 5450 50  0001 C CNN
F 3 "~" H 3700 5450 50  0001 C CNN
	1    3700 5450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3550 5450 3450 5450
Connection ~ 3450 5450
$Comp
L Device:L L3
U 1 1 5EEA8561
P 4150 5100
F 0 "L3" V 4400 5100 50  0000 C CNN
F 1 "47uH" V 4300 5100 50  0000 C CNN
F 2 "regulator5vto1:IND_1255AY-470M=P3" H 4150 5100 50  0001 C CNN
F 3 "~" H 4150 5100 50  0001 C CNN
	1    4150 5100
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EEAB263
P 3600 5100
AR Path="/5E9C8987/5EEAB263" Ref="C?"  Part="1" 
AR Path="/5EA6B564/5EEAB263" Ref="C57"  Part="1" 
F 0 "C57" V 3850 5100 50  0000 C CNN
F 1 "0.15uF" V 3750 5100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3600 5100 50  0001 C CNN
F 3 "~" H 3600 5100 50  0001 C CNN
	1    3600 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 5100 3500 5100
Wire Wire Line
	3700 5100 3900 5100
Wire Wire Line
	4300 5100 4350 5100
Wire Wire Line
	3400 5200 3700 5200
Wire Wire Line
	3700 5200 3700 5100
Wire Wire Line
	3850 5450 4350 5450
Wire Wire Line
	4350 5450 4350 5100
Connection ~ 4350 5100
Wire Wire Line
	4350 5100 4400 5100
Connection ~ 3700 5100
$Comp
L Device:C_Small C?
U 1 1 5EEBFF77
P 4350 5600
AR Path="/5E9C8987/5EEBFF77" Ref="C?"  Part="1" 
AR Path="/5EA6B564/5EEBFF77" Ref="C59"  Part="1" 
F 0 "C59" H 4250 5550 50  0000 R CNN
F 1 "22uF" H 4250 5600 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4350 5600 50  0001 C CNN
F 3 "~" H 4350 5600 50  0001 C CNN
	1    4350 5600
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 5500 4350 5450
Connection ~ 4350 5450
Wire Wire Line
	4350 5700 4350 5750
Wire Wire Line
	4350 5750 3900 5750
Connection ~ 3450 5750
$Comp
L Device:D_Schottky D10
U 1 1 5EECA7C9
P 3900 5250
F 0 "D10" V 3850 5350 50  0000 L CNN
F 1 "D_Schottky" V 3900 5350 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323" H 3900 5250 50  0001 C CNN
F 3 "~" H 3900 5250 50  0001 C CNN
	1    3900 5250
	0    1    1    0   
$EndComp
Connection ~ 3900 5100
Wire Wire Line
	3900 5100 4000 5100
Wire Wire Line
	3900 5400 3900 5750
Connection ~ 3900 5750
Wire Wire Line
	3900 5750 3450 5750
$Comp
L power:GND #PWR0201
U 1 1 5EEE3F1C
P 3050 4150
F 0 "#PWR0201" H 3050 3900 50  0001 C CNN
F 1 "GND" H 3050 4000 50  0000 C CNN
F 2 "" H 3050 4150 50  0001 C CNN
F 3 "" H 3050 4150 50  0001 C CNN
	1    3050 4150
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:LM2842Y U12
U 1 1 5EEE3F22
P 2600 3600
F 0 "U12" H 2600 4000 50  0000 C CNN
F 1 "LM2842Y" H 2600 3900 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-6" H 2625 3350 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2842-q1.pdf" H 2500 3700 50  0001 C CNN
	1    2600 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3700 2200 3500
Connection ~ 2200 3500
Wire Wire Line
	2200 3500 2000 3500
Wire Wire Line
	2600 3900 2000 3900
$Comp
L Device:C_Small C?
U 1 1 5EEE3F2D
P 2000 3700
AR Path="/5E9C8987/5EEE3F2D" Ref="C?"  Part="1" 
AR Path="/5EA6B564/5EEE3F2D" Ref="C54"  Part="1" 
F 0 "C54" H 2092 3746 50  0000 L CNN
F 1 "2.2uF" H 2092 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2000 3700 50  0001 C CNN
F 3 "~" H 2000 3700 50  0001 C CNN
	1    2000 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3600 2000 3500
Connection ~ 2000 3500
Wire Wire Line
	2000 3500 1850 3500
Wire Wire Line
	2000 3800 2000 3900
Connection ~ 2000 3900
Wire Wire Line
	2000 3900 1850 3900
$Comp
L Device:R R26
U 1 1 5EEE3F3A
P 3050 4000
F 0 "R26" H 3150 4050 50  0000 L CNN
F 1 "1K" H 3150 4000 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2980 4000 50  0001 C CNN
F 3 "~" H 3050 4000 50  0001 C CNN
	1    3050 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3700 3050 3700
Wire Wire Line
	3050 3700 3050 3850
Wire Wire Line
	3050 4150 1850 4150
$Comp
L Device:R R27
U 1 1 5EEE3F44
P 3300 3850
F 0 "R27" V 3400 3850 50  0000 C CNN
F 1 "30K" V 3300 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3230 3850 50  0001 C CNN
F 3 "~" H 3300 3850 50  0001 C CNN
	1    3300 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3150 3850 3050 3850
Connection ~ 3050 3850
$Comp
L Device:L L2
U 1 1 5EEE3F4D
P 3750 3500
F 0 "L2" V 4000 3500 50  0000 C CNN
F 1 "47uH" V 3900 3500 50  0000 C CNN
F 2 "regulator5vto1:IND_1255AY-470M=P3" H 3750 3500 50  0001 C CNN
F 3 "~" H 3750 3500 50  0001 C CNN
	1    3750 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EEE3F54
P 3200 3500
AR Path="/5E9C8987/5EEE3F54" Ref="C?"  Part="1" 
AR Path="/5EA6B564/5EEE3F54" Ref="C56"  Part="1" 
F 0 "C56" V 3450 3500 50  0000 C CNN
F 1 "0.15uF" V 3350 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3200 3500 50  0001 C CNN
F 3 "~" H 3200 3500 50  0001 C CNN
	1    3200 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 3500 3100 3500
Wire Wire Line
	3300 3500 3500 3500
Wire Wire Line
	3900 3500 3950 3500
Wire Wire Line
	3000 3600 3300 3600
Wire Wire Line
	3300 3600 3300 3500
Wire Wire Line
	3450 3850 3950 3850
Wire Wire Line
	3950 3850 3950 3500
Connection ~ 3950 3500
Connection ~ 3300 3500
$Comp
L Device:C_Small C?
U 1 1 5EEE3F65
P 3950 4000
AR Path="/5E9C8987/5EEE3F65" Ref="C?"  Part="1" 
AR Path="/5EA6B564/5EEE3F65" Ref="C58"  Part="1" 
F 0 "C58" H 3850 3950 50  0000 R CNN
F 1 "22uF" H 3850 4000 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3950 4000 50  0001 C CNN
F 3 "~" H 3950 4000 50  0001 C CNN
	1    3950 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3950 3900 3950 3850
Connection ~ 3950 3850
Wire Wire Line
	3950 4100 3950 4150
Wire Wire Line
	3950 4150 3500 4150
Connection ~ 3050 4150
$Comp
L Device:D_Schottky D1
U 1 1 5EEE3F71
P 3500 3650
F 0 "D1" V 3450 3750 50  0000 L CNN
F 1 "D_Schottky" V 3500 3750 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323" H 3500 3650 50  0001 C CNN
F 3 "~" H 3500 3650 50  0001 C CNN
	1    3500 3650
	0    1    1    0   
$EndComp
Connection ~ 3500 3500
Wire Wire Line
	3500 3500 3600 3500
Wire Wire Line
	3500 3800 3500 4150
Connection ~ 3500 4150
Wire Wire Line
	3500 4150 3050 4150
Wire Wire Line
	1850 3600 1850 3900
Connection ~ 1850 3900
Wire Wire Line
	1850 3900 1850 4150
Wire Wire Line
	4750 4150 4750 3700
Wire Wire Line
	4550 3300 4550 3500
Wire Wire Line
	3950 3500 4550 3500
Wire Wire Line
	3950 4150 4750 4150
Connection ~ 3950 4150
$Comp
L power:GNDS #PWR?
U 1 1 5EF341D2
P 3450 5750
AR Path="/5EA6A304/5EF341D2" Ref="#PWR?"  Part="1" 
AR Path="/5EA6B564/5EF341D2" Ref="#PWR0188"  Part="1" 
F 0 "#PWR0188" H 3450 5500 50  0001 C CNN
F 1 "GNDS" H 3500 5550 50  0000 C CNN
F 2 "" H 3450 5750 50  0001 C CNN
F 3 "" H 3450 5750 50  0001 C CNN
	1    3450 5750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
