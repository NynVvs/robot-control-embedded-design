EESchema Schematic File Version 4
LIBS:RobotIM-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4300 2550 900  1450
U 5EA55D74
F0 "Omega2+" 50
F1 "Omega2.sch" 50
F2 "JTAG[0..5]" B R 5200 3350 50 
$EndSheet
$Sheet
S 5700 4200 1250 1250
U 5EA69A87
F0 "FPGA ICE40" 50
F1 "FPGA_ICE40.sch" 50
F2 "PWM[0..3]" I R 6950 4750 50 
F3 "SPIF[0..3]" B R 6950 4450 50 
$EndSheet
Wire Bus Line
	6950 3750 7150 3750
Wire Bus Line
	7150 3750 7150 4450
$Sheet
S 7850 3300 1200 600 
U 5EA6A304
F0 "Motores Orugas" 50
F1 "Motores_Orugas.sch" 50
F2 "SPID[0..4]" B L 7850 3400 50 
F3 "PWM[0..3]" B L 7850 3550 50 
$EndSheet
Wire Bus Line
	6950 4450 7150 4450
$Sheet
S 4300 4200 950  1250
U 5EA6B564
F0 "Baterias" 50
F1 "Baterias.sch" 50
$EndSheet
Wire Bus Line
	6950 3400 7850 3400
Wire Bus Line
	7600 3550 7850 3550
Wire Bus Line
	6950 3550 7600 3550
Connection ~ 7600 3550
Wire Bus Line
	7600 4750 7600 3550
Wire Bus Line
	6950 4750 7600 4750
Wire Bus Line
	5700 3350 5200 3350
$Sheet
S 5700 2550 1250 1450
U 5E9C8987
F0 "STM32F412RG." 47
F1 "STM32F412RG.sch" 50
F2 "JTAG[0..5]" B L 5700 3350 50 
F3 "SPID[0..4]" B R 6950 3400 50 
F4 "SPIF[0..3]" B R 6950 3750 50 
F5 "PWM[0..3]" B R 6950 3550 50 
$EndSheet
$EndSCHEMATC
